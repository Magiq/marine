<?php

namespace Framework;

class ServiceContainer{

    private $services;

    public function bind($name, callable $init, $arguments = [])
    {
        $this->services[$name] = [$init, $arguments];
    }

    public function instance($name, $obj) {
        $this->services[$name] = $obj;
    }


    public function get($name) {
        if(!array_key_exists($name, $this->services)) throw new \Exception(sprintf('Trying to get not exists service %s', $name));

        //Check if not instance
        if(is_array($this->services[$name])) {
            list($func, $args) = $this->services[$name];
            return $func($args);
        }

        return $this->services[$name];
    }
}