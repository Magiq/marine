<?php

namespace Framework\Tests;

use PHPUnit\Framework\TestCase;
use Framework\Route\Router;
use Framework\Route\Route;
use Framework\Http\Request;
use Framework\Kernel;
use Framework\Http\Response;
use Framework\Controller;

class TestKernel extends Kernel{
    public function getAppDir()
    {
        return __DIR__ . DS . '../../app';
    }
}

class TestController extends Controller
{

    function hello(Request $request) 
    {
        return new Response('hello');
    }
}

class KernelTest extends TestCase
{

    public function testResponse()
    {
        $tests = [
            '/test' => [TestController::class, 'hello'],
            '/test' => function(Request $request) {
                return new Response('hello');
            },
            '/test' => TestController::class . ':hello'
        ];

        foreach($tests as $path=>$route)
        {
            $this->assertInstanceOf(Response::class, $this->getResponse($path, $route));
        }
    }

    public function testException()
    {
        $tests = [
            '/test' => [[TestController::class, 'hello', 'dafuck'], \Exception::class],
            '/test' => [TestController::class . '\hello', \Exception::class]
        ];

        foreach($tests as $path=>$params)
        {
            $this->expectException($params[1]);
            $this->getResponse($path, $params[0]);
        }
    }

    private function getResponse($path, $controller) {
        $router = new Router();
        $route = new Route($path, $controller);
        $router->add($route);
        $request = new Request(['REQUEST_URI' => $path]);
        $kernel = new TestKernel(true, $router);
        return $kernel->handle($request);
    }
}