<?php

namespace Framework\Tests\Http;

use PHPUnit\Framework\TestCase;
use Framework\Http\Request;

class RequestTest extends TestCase
{
    public function testPath()
    {
        $_server = [
            'REQUEST_URI' => '/some/4'
        ];

        $request = new Request($_server);
        $this->assertEquals('/some/4', $request->getPath());
    }

    public function testException()
    {
        $this->expectException(\Exception::class);
        $request = new Request([]);
        $request->getPath();
    }
}