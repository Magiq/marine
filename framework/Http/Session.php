<?php

namespace Framework\Http;

/**
 * Simple implementation in OOP manner sessions
 * 
 */
class Session
{
    private $_session;

    public function __construct()
    {
        session_start();
        $this->_session = &$_SESSION;
    }

    public function get($name)
    {
        return (array_key_exists($name, $this->_session)) ? $this->_session[$name] : null;
    }

    public function set($name, $value)
    {
        return $this->_session[$name] = $value;
    }
}