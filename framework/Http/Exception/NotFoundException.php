<?php

namespace Framework\Http\Exception;

use Framework\Http\Exception\HttpException;

class NotFoundException extends HttpException {
    public function getStatusCode(){ return 404; }
    public function getTypical() { return 'Not found'; }
}