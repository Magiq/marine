<?php

namespace Framework\Route\Exception;

class RouteNotFoundException extends \Exception {}