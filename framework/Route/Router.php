<?php

namespace Framework\Route;

use Framework\Router\RouterCollection;
use Framework\Http\Request;
use Framework\Route\Exception\RouteNotFoundException;
use Framework\Route\Route;

/**
 * Class provides basic routing system
 * 
 */
class Router
{
    private $routes;

    /**
     * Adding new routes
     * 
     * @param Route $route
     * @return $this
     */
    public function add(Route $route)
    {
        $path = ($route->getPath()) ? $route->getPath() : '_index';

        unset($this->routes[$path]);
        $this->routes[$path] = $route;

        return $this;
    }

    /**
     * Return controller by request
     * 
     * @param Request $request
     * @return Route with matched parameter
     */
    public function match(Request $request)
    {
        if(array_key_exists($request->getPath(), $this->routes)) {
            return $this->routes[$request->getPath()]; 
        } else {
            foreach($this->routes as $path=>$route) {
                if($this->compare($route, $request))
                    return $route;
            }
        }
       
        throw new RouteNotFoundException(sprintf('Route with "%s" path not found', $request->getPath()));
    }

    /**
     * Compare route with request path
     * 
     * @param Route $route
     * @param Request $request
     * @return boolean false if route not matched and true otherwise
     */
    private function compare(Route &$route, Request $request) {
        $route_path_parts = explode('/', $route->getPath());
        $request_path_parts = explode('/', $request->getPath());

        if(count($route_path_parts) != count($request_path_parts)) {
            return false;
        }

        for($i=0; $i<count($route_path_parts); $i++) {
            if(preg_match('/\{(\w+)(?:\:(int))?\}/', $route_path_parts[$i], $matches)) {
                if(isset($matches[2]) && is_numeric($matches[1])) {
                    $route->addParameter(intval($matches[1]), $request_path_parts[$i]);
                } else {
                    $route->addParameter($matches[1], $request_path_parts[$i]);
                }                
            }elseif($route_path_parts[$i] != $request_path_parts[$i]) {
                return false;
            }
        }

        return true;
    }
}