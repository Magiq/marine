<?php

namespace Framework;
use Framework\Http\InternalServerError;
use Framework\Http\Exception\HttpException;
use Framework\Http\Response;

/**
 * Handles all errors and exceptions raises in framework
 *  
 */
class ErrorHandler
{
    
    static function register()
    {
        set_error_handler([ErrorHandler::class, 'errorHandler']);
        set_exception_handler([ErrorHandler::class, 'exceptionHandler']);
    }

    static function errorHandler($errno , $errstr,  $errfile, $errline, array $errcontext)
    {
        $message = (DEBUG) ? sprintf("%s:%d %s", $errfile, $errline, $errstr) : 'InternalServerError';

        if (php_sapi_name() === 'cli') {
            throw new \Exception($message);
        }

        $request = new Response($message, 500);
        $request->send();
    }

    static function exceptionHandler(\Throwable $ex)
    {
        if($ex instanceof HttpException) {
            $message = (DEBUG) ? sprintf("%s:%d throw %s with message %s\n%s", $ex->getFile(), $ex->getLine(), get_class($ex), $ex->getMessage(), $ex->getTraceAsString()) : $ex->getTypical();

            if (php_sapi_name() === 'cli') {
                throw new \Exception($message);
            }

            $request = new Response('<pre>' . $message . '</pre>', $ex->getStatusCode());
        } else {
            $message = (DEBUG) ? sprintf("%s:%d %s('%s') \n%s", $ex->getFile(), $ex->getLine(), get_class($ex), $ex->getMessage(), $ex->getTraceAsString()) : 'InternalServerError';

            if (php_sapi_name() === 'cli') {
                throw new \Exception($message);
            }

            $request = new Response('<pre>' . $message . '</pre>', 500);
        }

        $request->send();
    }
}