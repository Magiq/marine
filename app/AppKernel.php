<?php

use Framework\Kernel;

class AppKernel extends Kernel
{

    protected function getAppDir() {
        return __DIR__;
    }

}