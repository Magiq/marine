<?php

namespace Controller;

use Framework\Controller;
use Framework\Http\Request;
use Framework\Http\Response;
use Model\User;
use Framework\Exception\ValidationException;

class UserController extends Controller{

    public function login(Request $request)
    {
        if($request->getMethod() == 'POST') {
            $data = $request->getPost();

            $user = $this->getModel(User::class);
            try {
                $exists = (bool)$user->login($data['username'], $data['password']);
            } catch(ValidationException $e) {
                return new Response($this->render('user/login.tpl', ['error' => $e->getMessage()]));
            }
            
            if(!$exists) {
                return new Response($this->render('user/login.tpl', ['error' => 'Wrong name or/and password']));
            }

            $session = $this->container->get('session');
            $session->set('loggined', true);
            $response = new Response();
            $response->redirect('/');
        }

        return new Response($this->render('user/login.tpl'));
    }

}