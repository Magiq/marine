<?php

use Framework\Route\Router;
use Framework\Route\Route;

$router = new Router();
$router->add(new Route('', [Controller\TaskController::class, 'index']))
    ->add(new Route('/tasks/add', [Controller\TaskController::class, 'add']))
    ->add(new Route('/tasks', [Controller\TaskController::class, 'index']))
    ->add(new Route('/tasks/{page:int}', [Controller\TaskController::class, 'index']))
    ->add(new Route('/tasks/edit/{page:int}', [Controller\TaskController::class, 'edit']))
    ->add(new Route('/image/upload', [Controller\ImageController::class, 'upload']))
    ->add(new Route('/tasks/preview', [Controller\TaskController::class, 'preview']))
    ->add(new Route('/login', [Controller\UserController::class, 'login']));

return $router;