drop table if exists tasks;
drop table if exists users;

create table tasks(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    username CHAR(50) NOT NULL,
    email CHAR(255) NOT NULL,
    text TEXT NOT NULL,
    images TEXT,
    is_done BOOLEAN NOT NULL DEFAULT 0 CHECK (is_done IN (0,1))
);

create table users(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    username CHAR(50) NOT NULL,
    password CHAR(32) NOT NULL
);

INSERT INTO USERS(username, password) VALUES('admin', '202cb962ac59075b964b07152d234b70');