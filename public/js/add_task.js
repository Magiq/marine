$(document).ready(function() {
    var images = [];

    $('#addImage').on('click', function(e) {
        e.preventDefault();
        var input = $('<input />').attr({type: 'file'})
        $('#imagesInput').append(input);
        $(input).change(function(e){
            var form_data = new FormData();
            form_data.append('image', e.target.files[0]);
            console.log(e.target.files[0]);

            $.ajax({
                url: '/image/upload',
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                type: 'post'
            }).done(function(response) {
                var li = $('<li />');
                var img = $('<img />').attr({src: response.image});
                var close = $('<a/>').attr({class: 'close glyphicon glyphicon-remove'});

                $(close).click(function(e) {
                    $(li).remove();
                    $('input[value="' + response.image + '"]').remove();
                });

                $(li).append(img);
                $(li).append(close);
                $('#imagePreview').append(li);

                var input = $('<input />').attr({type: 'hidden', name: 'images[]', value: response.image});
                $('#addForm').append(input);
                $(e.target).remove();
            })
        });
        images.push(input);
    });

    $('#previewButton').on('click', function(e) {
        e.preventDefault(); // Prevent the form from submitting via the browser

        var data = $('#addForm').serializeArray();

        $.post({
            url: '/tasks/preview',
            dataType: 'html',
            data: data
        }).done(function(response) {
            $('#preview').html(response);
            console.log(response);
        });
    });
});